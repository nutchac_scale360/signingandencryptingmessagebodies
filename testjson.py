import requests
import json
from sign_verify import sign_verify

#rsa=sign_verify()
url = "http://172.16.111.55:8082/api/v1/authorizes"

#payload = "{\"data\":{\"phone_no\":\"08494298109\",\"pin_code\":\"1234\"},\"encryption\":\"A5FMUmNPRAmb5Rptukd5LB18C4XPOC+tIKBvE2DkVCaC9G5eI0w2rNrDBpdBQbTQwT6SClzU0noGvW8TK9NqHhh8gC5b+aBQnc24YxypCU3BuKliG3adaODurnjMTDbinh+9DyjSHzJ1E1FIK/LLRCN4t2ZvxLZJpmxUj788FXEN7pRA2Zo0nTIg6TelNldwwHrzUTKhK/zIc81XL4qigKLiTmwcTib+NF3J/ISo2aw/aFZoJorpvWcZYUdGWuv3oOX7iDIINEhuSSrJcLhtm6XpEqkZzLxC2dksM5Yp+KpoCqcgMT02epBDyLthvlWoTFUK2Y2nR2iJfIWy9DeP6A==\"}"

data={"phone_no":"08494298109","pin_code":"1234"}
rawData={'data':data,'encryption':'A5FMUmNPRAmb5Rptukd5LB18C4XPOC+tIKBvE2DkVCaC9G5eI0w2rNrDBpdBQbTQwT6SClzU0noGvW8TK9NqHhh8gC5b+aBQnc24YxypCU3BuKliG3adaODurnjMTDbinh+9DyjSHzJ1E1FIK/LLRCN4t2ZvxLZJpmxUj788FXEN7pRA2Zo0nTIg6TelNldwwHrzUTKhK/zIc81XL4qigKLiTmwcTib+NF3J/ISo2aw/aFZoJorpvWcZYUdGWuv3oOX7iDIINEhuSSrJcLhtm6XpEqkZzLxC2dksM5Yp+KpoCqcgMT02epBDyLthvlWoTFUK2Y2nR2iJfIWy9DeP6A=='}
print(rawData)

payload=json.dumps(rawData)

print(payload)


headers = {
    'Content-Type': "application/json",
    'deviceId': "dba82d29-6b98-4cfa-81a8-81e0a7d075c0"
    }

response = requests.request("PUT", url, data=payload, headers=headers)

print(response.text)
