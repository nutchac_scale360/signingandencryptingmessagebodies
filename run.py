from base64 import (
    b64encode,
    b64decode,
)
from sign_verify import sign_verify
import requests
import json
class run:
    def __init__(self):
        self.rsa=sign_verify()


    def start(self):
        private_key=self.rsa.ReadStrMyPrivateKey()
        print ("private_key: \n",private_key,"\n")

        public_key=self.rsa.ReadStrMyPublicKey()#.replace("\n","").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "")
        print ("Public Key: \n",public_key,"\n")
        
        print('----------------------------------------------------')

        str="hello!"
        print ("Str: ",str,"\n")

        ciphertext=self.rsa.encryptionPKCS(str)
        print ("encryption Str: ",ciphertext,"\n")
        detext=self.rsa.decrypt(ciphertext)
        print ("decrypt Str: ",detext,"\n")
        
        signature=self.rsa.SignatureSHA256(str)
        print ("My signature: ", signature,"\n")
        
        print('----------------------------------------------------')
        
        
        jsonData={"name":"Test1111","public_key":public_key}
        print ("Str to send device: ",jsonData,"\n")
        ry=requests.post('http://172.16.111.55:8084/api/v1/devices',data=json.dumps(jsonData),headers={"Content-Type" : "application/json"})
        print('connect status:',ry.status_code,"\n")
        device = ry.json()
        print('device:',device['data']['id'],"\n")
        
        strData = {'phone_no':'08494298109','pin_code':'1234'}
        print ("Str: ",strData,"\n")
        
        encrypt = self.rsa.encryptionPKCS(strData)
        print ("encryption Str: ",encrypt,"\n")
        signature=self.rsa.SignatureSHA256(encrypt)
        print ("encryption Str1: ",signature,"\n")
        signature="A5FMUmNPRAmb5Rptukd5LB18C4XPOC+tIKBvE2DkVCaC9G5eI0w2rNrDBpdBQbTQwT6SClzU0noGvW8TK9NqHhh8gC5b+aBQnc24YxypCU3BuKliG3adaODurnjMTDbinh+9DyjSHzJ1E1FIK/LLRCN4t2ZvxLZJpmxUj788FXEN7pRA2Zo0nTIg6TelNldwwHrzUTKhK/zIc81XL4qigKLiTmwcTib+NF3J/ISo2aw/aFZoJorpvWcZYUdGWuv3oOX7iDIINEhuSSrJcLhtm6XpEqkZzLxC2dksM5Yp+KpoCqcgMT02epBDyLthvlWoTFUK2Y2nR2iJfIWy9DeP6A=="
        print ("encryption Str2: ",signature,"\n")
        
        data = {"phone_no":"08494298109","pin_code":"1234"}
        jsonData = {"data":data,"encryption":signature}
        print ("Str to send verifily: ",jsonData,"\n")
        
        
        r=requests.put('http://172.16.111.55:8082/api/v1/authorizes',data=json.dumps(jsonData),headers = {'Content-Type': "application/json",'deviceId': device['data']['id']})
        print('connect status:',r.status_code,"\n")
        aa=r.json()
        print(aa)


        print('----------------------------------------------------')

        str1="qqqqqqqqq"
        print ("verifierPublicKey Str: ",str1,"\n")
        if(self.rsa.verifierPublicKey('PublicKey.pem',str1)):
            print ("Successfully verified message\n")
        else:
            print("Signature verification failed\n")

        str2="hello"
        print ("verifierPublicKey Str: ",str2,"\n")
        if(self.rsa.verifierPublicKey('PublicKey.pem',str2)):
             print ("Successfully verified message\n")
        else:
            print("Signature verification failed\n")
a=run()
a.start()    
