
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import (rsa,padding,utils)
from cryptography.hazmat.primitives import (hashes,serialization)
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.exceptions import InvalidSignature

from base64 import (
    b64encode,
    b64decode,
)
import requests
import json

class sign_cryptography():
	
	def load_key(self,filename):
		with open(filename, 'rb') as pem_in:
			pemlines = pem_in.read()
		self.myPrivateKey = load_pem_private_key(pemlines, None, default_backend())
		self.myPublicKey=self.myPrivateKey.public_key()
	
	def loadStrPublicKey(self):
		pem = self.myPublicKey.public_bytes(
			encoding=serialization.Encoding.PEM,
			format=serialization.PublicFormat.SubjectPublicKeyInfo,
		)
		str1=str(pem,'utf-8')
		str1=str1.replace("\n","").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "")
		return str1
	
		
	def Signature(self,message):
		message=bytes(message,'utf-8')
		self.signature = self.myPrivateKey.sign(
			message,
			padding.PSS(
				mgf=padding.MGF1(hashes.SHA256()),
				salt_length=padding.PSS.MAX_LENGTH
			),
			hashes.SHA256()
		)
		signature=b64encode(self.signature)
		signature=str(signature,'utf-8')
		signature=signature.replace("\n","")
		return signature
		
	def verifierPublicKey(self,message):
		try:
			message=bytes(message,'utf-8')
			self.myPublicKey.verify(
				self.signature,
				message,
				padding.PKCS1v5(
					mgf=padding.MGF1(hashes.SHA256()),
					salt_length=padding.PSS.MAX_LENGTH
				),
				hashes.SHA256()
			)
			return True
		except InvalidSignature:
			return False
	
	def encryptionPKCS(self,message):
		message=bytes(message,'utf-8')
		'''
		ciphertext = self.myPublicKey.encrypt(
			message,
			padding.OAEP(
				mgf=padding.MGF1(algorithm=hashes.SHA256()),
				algorithm=hashes.SHA256(),
				label=None
			)
		)
		'''
		ciphertext = self.myPublicKey.encrypt(
			message,
			padding.PKCS1v15()
		)
		ciphertext=b64encode(ciphertext)
		ciphertext=str(ciphertext,'utf-8')
		return ciphertext



a=sign_cryptography()
filename = "private_key_pkcs8.pem"
a.load_key(filename)
strPublic_key=a.loadStrPublicKey()


jsonData={"name":"Test1111","public_key":strPublic_key}
print ("Str to send device: ",jsonData,"\n")

ry=requests.post('http://172.16.111.55:8084/api/v1/devices',data=json.dumps(jsonData),headers={"Content-Type" : "application/json"})
print('connect status:',ry.status_code,"\n")
device = ry.json()
print('device:',device['data']['id'],"\n")




print("--------------------------------------------------------------------------")


        
strData = {'phone_no':'08494298109','pin_code':'1234'}
strData=json.dumps(strData)
print ("Str: ",strData,"\n")

signature=a.Signature(strData)
print ("Signature Str: ",signature,"\n")

data = {"phone_no":"08494298109","pin_code":"1234"}
jsonData = {"data":data,"encryption":signature}
print ("Str to send verifily: ",jsonData,"\n")

r=requests.put('http://172.16.111.55:8082/api/v1/authorizes',data=json.dumps(jsonData),headers = {'Content-Type': "application/json",'deviceId': device['data']['id']})
print('connect status:',r.status_code,"\n")
aa=r.json()
print(aa)

