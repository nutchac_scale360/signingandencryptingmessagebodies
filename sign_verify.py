#!/usr/bin/env python

from base64 import (
    b64encode,
    b64decode,
)
from Crypto import Random
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Cipher import (
                            AES,
                            PKCS1_OAEP
                            )

class sign_verify():
    def __init__(self):
        self.myPrivateKey = False
        print("Create Private Key ...")
        #self._CreateMyPrivateKey()
        self._ReadMyPrivateKey()
        self._CreateMyPublicKey()
        
    #def _CreateMyPrivateKey(self):
        #self.myPrivateKey=RSA.generate(2048)
        #file=open('PrivateKey.pem','w')
        #strKey=str(self.myPrivateKey.exportKey('PEM',8),'utf-8')
        #file.write(strKey)
        #file.close()

    def _ReadMyPrivateKey(self):
        # Read shared key from file
        self.myPrivateKey = False
        with open ("private_key_pkcs8.pem", "r") as myfile:
            self.myPrivateKey = RSA.importKey(myfile.read())
        return self.myPrivateKey
    
    def ReadStrMyPrivateKey(self):
        self._ReadMyPrivateKey()
        #return str(self.myPrivateKey.exportKey('PEM',8),'utf-8')
        strPri=RSA.exportKey('PEM')
        #strPri=strPri.replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "")
        return strPri
    
    def _CreateMyPublicKey(self):
        self.myPublicKey=self.myPrivateKey.publickey()
        file=open('public_key_pkcs8.pem','w')
        strKey=str(self.myPublicKey.exportKey('PEM',1),'utf-8')
        file.write(strKey)
        file.close()
        
    def _ReadMyPublicKey(self):
        # Read shared key from file
        self.myPublicKey = False
        with open ("public_key_pkcs8.pem", "r") as myfile:
            self.myPublicKey = RSA.importKey(myfile.read())
        return self.myPublicKey
    
    def ReadStrMyPublicKey(self):
        self._ReadMyPublicKey()
        return str(self.myPublicKey.exportKey('PEM',1),'utf-8')
        
    def _CreateHashSHA256(self,message):
        self.hash = SHA256.new()
        self.hash.update(bytes(message,'utf-8'))
        
    def SignatureSHA256(self,message):
        # Load private key and sign message
        signer = PKCS1_v1_5.new(self.myPrivateKey)
        self._CreateHashSHA256(message)
        self.signature = signer.sign(self.hash)
        signature=b64encode(self.signature)
        return signature.decode('utf-8')	#convert byte to str
    
    def verifierPublicKey(self,urlReceivePublicKey,message):
        # Load public key and verify message

        self.ReceivePublicKey = RSA.importKey(open(urlReceivePublicKey).read())
        verifier = PKCS1_v1_5.new(self.ReceivePublicKey)
        
        self._CreateHashSHA256(message)
#        signature=b64decode(signature)
        signature=self.SignatureSHA256(message)
        verified = verifier.verify(self.hash, signature)
        if not verified:
#            print('Signature verification failed')
            return True
        else:
#            print ('Successfully verified message')
            return False
 
    def encryptionPKCS(self,message):
        cipher = PKCS1_OAEP.new(self.myPrivateKey)
        self._CreateHashSHA256(message)
        message=bytes(message,'utf-8')
        ciphertext = cipher.encrypt(message)
        ciphertext=b64encode(ciphertext)
        return ciphertext.decode('utf-8')
        
    
    def decrypt(self,enc_data):
        enc_data=b64decode(enc_data)
        cipher = PKCS1_OAEP.new(self.myPrivateKey)
        de_data=cipher.decrypt(enc_data)
        return de_data.decode('utf-8')
        
